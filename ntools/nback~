#!/usr/bin/python

# NBack
# Neurology Backup Script

# Written by Colin Humphries
# April, 2017

# Current Version 1.0.7
# Changes
# 1.0.7 - 7/06/17 - Fixed HTML formatting bug
# 1.0.6 - 7/05/17 - Fixed ctm datetime bug. Added file/dir stats to status output (cjh)
# 1.0.5 - 6/27/17 - Added current version to report. Cleaned up some code (cjh)
# 1.0.4 - 6/22/17 - Added current version file (cjh)
# 1.0.3 - 6/21/17 - Add python argparse. Removed email functionality (should be in
#                   separate script. Added html output to status (cjh)
# 1.0.2 - 6/15/17 - Added -status and -status-all options (cjh)

import sys
import subprocess
import os
import datetime
import time
import glob
import logging
import argparse

nback_version = '1.0.7'

configfile = '/usr/local/etc/nback.conf'
lockfile = '.lock_nback'
versionfile = '.version_nback'
rsync_cmd = '/usr/bin/rsync'
rsync_backup_cmdargs = [rsync_cmd,'-ahRvi','--stats','--force','--numeric-ids',
                         '--delete','--delete-excluded','--one-file-system',
                         '--exclude=*/.local/share/Trash',
                         '--exclude=*/.cache']
cp_cmdargs = ['/bin/cp','-al']


# The following class contains configuration parameters loaded 
# from configfile
# srcdir: colon separated list of directories to include in the backup, 
#         e.g. /home:/etc
# destdir: single directory where the backups will be saved. 
#          Note: right now this directory should be mounted with NFS, but 
#          future versions should support backup over SSH
# logdir: single directory where the log files will be saved.
# enabled: ON or OFF. Will turn the backups on or off.
# Note: the following variable define when backups will be deleted.
#       D = number of days between the backup and the current time
#       N = total number of saved backups
#       A backup is deleted if:
#         (N > max_backups) OR (N > min_backups AND D > period) 
# period: 
# min_backups:
# max_backups:
# max_delete: total number of backups to delete at any given time. This is to
#             keep the script from deleting everything if it has not run in awhile.
# parse_error: Internal variable for recording whether there is a parsing error.

class NBConfig:
    def __init__(self):
        self.srcdir = None
        self.destdir = None
        self.logdir = None
        self.enabled = False
	self.period = 7
	self.min_backups = 7
	self.max_backups = 14
	self.max_delete = 1
        self.parse_error = False

    def parse_file(self,filename):
        fin = open(filename,'r')
        lines = fin.readlines()
        fin.close()
        for line in lines:
            line = line.strip()
            if len(line) == 0:
                continue
            if line[0] == '#':
                continue
            sind = line.find('=')
            if sind < 0:
                self.parse_error = True
                continue
            vname = line[:sind].strip().lower()
            vvalue = line[(sind+1):].strip()
            if vname == 'srcdir':
                self.srcdir = vvalue.split(':')
            elif vname == 'destdir':
                self.destdir = vvalue
            elif vname == 'logdir':
                self.logdir = vvalue
            elif vname == 'enabled':
                vvalue = vvalue.lower()
                if vvalue == 'on':
                    self.enabled = True
                elif vvalue == 'off':
                    self.enabled = False
                else:
                    self.parse_error = True
	    elif vname == 'period':
		self.period = int(vvalue)
	    elif vname == 'min_backups':
		self.min_backups = int(vvalue)
	    elif vname == 'max_backups':
		self.max_backups = int(vvalue)
	    elif vname == 'max_delete':
		self.max_delete = int(vvalue)


# Parse input arguments
parser = argparse.ArgumentParser(description='Neurology backup script')
parser.add_argument('-backup', action="store_true", default=False, 
                    help='Start backup on current machine')
parser.add_argument('-unlock', action="store_true", default=False, 
                    help='Unlock backup directory for current machine')
parser.add_argument('-status', action="store_true", default=False, 
                    help='Print backup status for current machine')
parser.add_argument('-status_all', action="store_true", default=False, 
                    help='Print backup status for all machines')
parser.add_argument('-html', action="store_true", default=False, 
                    help='Output in html format')
parser.add_argument('-nohead', action="store_true", default=False, 
                    help='Output only the html body')

args = parser.parse_args()

if len(sys.argv) < 2:
    parser.print_usage()
    sys.exit()

# Load configuration file
nbc = NBConfig()
nbc.parse_file(configfile)
if nbc.parse_error:
    print 'Error parsing config file'
    sys.exit(1)

# Check for some necessary information
if nbc.srcdir is None:
    print "Source directories not specified in " + configfile + " file. Exiting."
    sys.exit()
if nbc.destdir is None:
    print "Destination directory not specified in " + configfile + " file. Exiting."
    sys.exit()
if len(nbc.srcdir) == 0:
    print "Error parsing source directory list in " + configfile + " file. Exiting."
    sys.exit()
if not os.path.exists(nbc.destdir):
    print "Error. Destination directory does not exist. Exiting."
    sys.exit(1)
if nbc.logdir is None:
    print "Log directory not specified in " + configfile + " file. Exiting."
    sys.exit()

# Get current time
ctm = datetime.datetime.now()

# Change to the destination directory
os.chdir(nbc.destdir)

# Output status to stdout
# output format can be either text or html
if args.status or args.status_all:
    # Get backup directory name
    mbasep,cmname = os.path.split(nbc.destdir)
    # Get list of machines
    if args.status_all:
        os.chdir(mbasep)
        mnames = glob.glob('*.*.*.*')
        mnames.sort()
    else:
        mnames = [cmname]
    # Check machine list length
    if len(mnames) == 0:
        print "Error: no machines found in backup directory"
        sys.exit(1)
    if args.html and not args.nohead:
        print "<html>\n<head></head>\n<body>"
    
    for mname in mnames:
        if args.html:
            print '<div>\n<h1 style="color:rgb(0,0,180);font-size:110%;">' + mname + '</h1>'
        else:
            print '****** ' + mname + ' ******'
        os.chdir(mbasep + '/' + mname)
        
        # Check whether the directory is locked
        if os.path.exists(lockfile):
            lstr = "locked"
        else:
            lstr = "unlocked"
        # Check nback version. This is used to keep track of what version
        # of the script is being used on each machine
        if os.path.exists(versionfile):
            fvin = open(versionfile,'r')
            vstr = fvin.readline()
            fvin.close()
        else:
            vstr = 'unknown'
            
        if args.html:
            print "<ul>"
            # print " <li>Backups are currently: <b>" + estr + "</b></li>"
            print " <li>Backup directory is currently: <b>" + lstr + "</b></li>"
            print " <li>NBack version is: <b>" + vstr + "</b></li>"
        else:
            # print "  * Backups are currently: " + estr
            print "  * Backup directory is currently: " + lstr
            print "  * NBack version is: " + vstr
            
        flist = glob.glob('2???.??.??-????')
        flist.sort()
        
        if len(flist) == 0:
            if args.html:
                print ' <li style="color:red;">No backup folders found</li>'
                print '</ul>'
                print '</div>'
            else:
                print '!!! No backup folders found'
            continue
        # Get information about backup directory size, number of files, and how much data
        # were changed during each backup from the rsync output in the log directory.
        fdata = []
        fdataslen = [5,4,7] # Variable to figure out max string length for text formating
        for fname in flist:
            if not os.path.exists('log/' + fname + '.changes'):
                fdata.append(['','',''])
                continue
            fnin = open('log/' + fname + '.changes','r')
            for fline in fnin:
                if fline[0:11] == 'Number of f':
                    # Number of files
                    fdnum = fline.split(':')[1].split()[0]
                elif fline[:7] == 'Total f':
                    # Directory size
                    fdsize = fline.split(':')[1].split()[0]
                elif fline[:7] == 'Total t':
                    # Size of transfered files
                    fdtrans = fline.split(':')[1].split()[0]
            fnin.close()
            fdata.append([fdnum,fdsize,fdtrans])
            # Get max string length
            if len(fdnum) > fdataslen[0]:
                fdataslen[0] = len(fdnum)
            if len(fdsize) > fdataslen[1]:
                fdataslen[1] = len(fdsize)
            if len(fdtrans) > fdataslen[2]:
                fdataslen[2] = len(fdtrans)
                
        if args.html:
            print " <li>List of backup folders"
            print '  <table style="margin-left:1em;">'
            print '   <tr><th style="text-align:left;">Date-Time</th><th style="text-align:left;">Files</th><th style="text-align:left;">Size</th><th style="text-align:left;">Changed</th></tr>'
            for fname,fdat in zip(flist,fdata):
                print '   <tr><td style="padding-right:1em;">{}</td><td style="padding-right:1em;">{}</td><td style="padding-right:1em;">{}</td><td style="padding-right:1em;">{}</td></tr>'.format(fname,fdat[0],fdat[1],fdat[2])
            print '  </table>'
            print ' </li>'
        else:
            print "  * List of backup folders:"            
            ostr = '      {:15}  {:' + str(fdataslen[0]) + \
                   '}  {:' + str(fdataslen[1]) + \
                   '}  {:' + str(fdataslen[2]) + '}'
            print ostr.format('Date-Time','Files','Size','Changed')
            for fname,fdat in zip(flist,fdata):
                print ostr.format(fname,fdat[0],fdat[1],fdat[2])

        # Figure out how much time has elapsed since last backup
        dctm = datetime.datetime.strptime(flist[-1],"%Y.%m.%d-%H%M")
        dt = ctm-dctm
        cmins,csecs = divmod(dt.seconds,60)
        chours,cmins = divmod(cmins,60)
        if not args.html:
            if dt.days >= 2:
                print '!!! It has been ' + str(dt.days) + ' days ' + str(chours) + \
                    ' hours and ' + str(cmins) + ' minutes since last backup'
            else:
                print '  * It has been ' + str(dt.days) + ' days ' + str(chours) + \
                    ' hours and ' + str(cmins) + ' minutes since last backup'
            print ''
        else:
            if dt.days >= 2:
                print ' <li style="color:red;">It has been ' + str(dt.days) + ' days ' + str(chours) + \
                    ' hours and ' + str(cmins) + ' minutes since last backup</li>'
            else:
                print ' <li>It has been ' + str(dt.days) + ' days ' + str(chours) + \
                    ' hours and ' + str(cmins) + ' minutes since last backup</li>'
            print '</ul>\n</div>'
        
    if args.html and not args.nohead:
        print "</body>\n</html>"
    sys.exit(0)
    
# Start logging
if not os.path.exists(nbc.logdir):
    os.mkdir(nbc.logdir)
logfilename = nbc.logdir + '/nback.log'
logging.basicConfig(filename=logfilename,format='%(levelname)s * %(asctime)s * %(message)s',level=logging.INFO)

# Save version file
# This is simply an easy way to keep track of what version of this script is running on
# each computer.
fout = open(versionfile,'w')
fout.write(nback_version)
fout.close()

if args.unlock:
    logging.info('Unlocking backup directory')
    if os.path.exists(lockfile):
        os.remove(lockfile)
        logging.info('Lock file removed')
    else:
        logging.info('Lock file not found. Directory is already unlocked.')
    
elif args.backup:
    if not nbc.enabled:
        print "Backups not enabled in " + configfile + " file. Exiting."
        sys.exit()

    logging.info('Starting backup')

    # Get current time
    dirname = ctm.strftime("%Y.%m.%d-%H%M")
    
    # Check for lock file
    if os.path.exists(lockfile):
        logging.error('Backup directory is locked. Aborting backup.')
        sys.exit()

    # Create lock file
    fout = open(lockfile,'w')
    fout.write(dirname)
    fout.close()


    if os.path.exists(dirname):
        # If the backup directory already exists, then wait 1 minute and try a new name.
        time.sleep(61)
        ctm = datetime.datetime.now()
        dirname = ctm.strftime("%Y.%m.%d-%H%M")

    flist = glob.glob('2???.??.??-????')
    flist.sort()

    if len(flist) > 0:
        # Copy last backup directory using hardlinks
        cmdargs = []
        cmdargs.extend(cp_cmdargs)
        cmdargs.append(flist[-1])
        cmdargs.append(dirname)
        logging.info('Copying previous backup ' + flist[-1] + ' to new directory ' + dirname)
        subprocess.call(cmdargs)
	# os.mkdir(dirname)
	# Now delete any old directories
	for ii in range(0,nbc.max_delete):
	    dctm = datetime.datetime.strptime(flist[ii],"%Y.%m.%d-%H%M")
	    dt = ctm-dctm
	    if (len(flist) > nbc.max_backups) or ((dt.days > nbc.period) and (len(flist) > nbc.min_backups)):
	        cmdargs = ['/bin/rm','-rf',flist[ii]]
		logging.info('Deleting directory ' + flist[ii])
		subprocess.call(cmdargs)
    else:
        # Otherwise just create the directory
	logging.warning('No previous backups found. Creating new directory.')
        os.mkdir(dirname)

    # Run rsync command
    cmdargs = []
    cmdargs.extend(rsync_backup_cmdargs)
    cmdargs.extend(nbc.srcdir)
    cmdargs.append(nbc.destdir + '/' + dirname)
    logging.info('Running rsync command: ' + ' '.join(cmdargs))
    rout = subprocess.Popen(cmdargs,stdout=subprocess.PIPE).communicate()[0]
    logging.info('Rsync finished')
    # Save rsync output
    fout = open(nbc.logdir + '/' + dirname + '.changes','w')
    fout.write(rout)
    fout.close()
    os.remove(lockfile)
